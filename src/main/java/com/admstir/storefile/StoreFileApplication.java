package com.admstir.storefile;

import com.admstir.storefile.models.User;
import com.admstir.storefile.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Arrays;
import java.util.Collections;
/**
 * @author Mostafa Moradi
 * @version 1.0.0
 * @since 2021/8/17
 */
@SpringBootApplication
@EnableMongoAuditing//for enable annotations such as @CrateDate
public class StoreFileApplication {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public StoreFileApplication(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public static void main(String[] args) {
        SpringApplication.run(StoreFileApplication.class, args);

    }

    @Autowired
    public void authenticationManager() {
        if (this.userRepository.findByUsername("mostafa1375") == null) {
            User user = new User("mostafa", "mostafa1375", passwordEncoder.encode("mostafa123"), Collections.singletonList("SUPER_ADMIN"),null);
            this.userRepository.save(user);
        }

    }
}
