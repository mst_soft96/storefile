package com.admstir.storefile.services;

import com.admstir.storefile.models.StoredFile;
import com.admstir.storefile.models.responses.FileResponseModel;
import com.admstir.storefile.models.responses.ResponseModel;
import com.admstir.storefile.repositories.StoredFileRepository;
import com.admstir.storefile.storages.StorageService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collections;
import java.util.Objects;

/**
 * @author Mostafa Moradi
 * @version 1.0.0
 * @since 2021/8/17
 */
@Service
public class FileService {
    private final ResponseModel responseModel;
    private final StoredFileRepository storedFileRepository;
    private final StorageService storageService;
    private final FileResponseModel fileResponseModel;
    private final UserService userService;

    @Autowired
    public FileService(ResponseModel responseModel, StoredFileRepository storedFileRepository, StorageService storageService, FileResponseModel fileResponseModel, UserService userService) {
        this.responseModel = responseModel;
        this.storedFileRepository = storedFileRepository;
        this.storageService = storageService;
        this.fileResponseModel = fileResponseModel;
        this.userService = userService;
    }

    public ResponseEntity<?> storeFile(MultipartFile file, Authentication authentication) {
        String format = Objects.requireNonNull(file.getOriginalFilename()).substring(file.getOriginalFilename().lastIndexOf("."));
        try {
            String username = authentication.getName();
            StoredFile storedFile = new StoredFile(storageService.store(file, format), format, username);
            String fileId = storedFileRepository.save(storedFile).getId();
            System.out.println(fileId);
            fileResponseModel.setId(fileId + "");
            userService.updateUserByUsername(username, fileId);
            return ResponseEntity.ok().body(fileResponseModel);
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
            return responseModel.responseModel(HttpStatus.BAD_REQUEST.value(), Collections.singletonList(exception.getMessage()));
        }

    }

    public String findFileById(String id) {
        StoredFile storedFile = storedFileRepository.findById(id).get();
        return storedFile.getName() + storedFile.getFormat();
    }
}
