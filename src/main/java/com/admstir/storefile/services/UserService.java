package com.admstir.storefile.services;

import com.admstir.storefile.models.User;
import com.admstir.storefile.models.requests.UserModel;
import com.admstir.storefile.models.responses.ResponseModel;
import com.admstir.storefile.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class UserService {
    private final UserRepository userRepository;
    private final ResponseModel responseModel;
    private final PasswordEncoder passwordEncoder;


    @Autowired
    public UserService(UserRepository userRepository, ResponseModel responseModel, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.responseModel = responseModel;
        this.passwordEncoder = passwordEncoder;
    }

    public ResponseEntity<?> userSore(UserModel userModel) {
        User user = new User(userModel.getName(), userModel.getUsername(), passwordEncoder.encode(userModel.getPassword()), userModel.getRoles(), null);
        try {
            userRepository.save(user);
            return ResponseEntity.ok().body("Welcome to StoreFile");

        } catch (Exception e) {
            return responseModel.responseModel(HttpStatus.BAD_REQUEST.value(), Collections.singletonList(e.getMessage()));
        }

    }

    public User getUserByUsername(String name) {

        return userRepository.findByUsername(name);
    }

    public void updateUserByUsername(String username, String fileId) {
        User user = getUserByUsername(username);
        try {
            List<String> filesId;
            if (user.getFilesId() == null)
                filesId = new ArrayList<>();
            else
                filesId = user.getFilesId();

            filesId.add(fileId);
            user.setFilesId(filesId);

        } catch (Exception exception) {
            System.out.println(exception.getMessage() + "****");
        }


        userRepository.save(user);


    }
}
