package com.admstir.storefile.configurations.exceptions;


import com.admstir.storefile.models.responses.ResponseModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Collections;
import java.util.NoSuchElementException;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
    private final ResponseModel responseModel;


    @Autowired
    public GlobalExceptionHandler(ResponseModel responseModel) {
        this.responseModel = responseModel;
    }




    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler({NoSuchElementException.class, DataIntegrityViolationException.class, InvalidDataAccessApiUsageException.class})
    public ResponseEntity<?> notFoundException(Exception exception) {

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body( responseModel.responseModel(HttpStatus.NOT_FOUND.value(), Collections.singletonList(exception.getMessage())));
    }

   /* @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<?> badRequestException(Exception exception) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body( responseModel.responseModel(HttpStatus.BAD_REQUEST.value(),exception.getMessage()));
    }
*/

    /*@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(InternalServerErrorException.class)
    public ResponseEntity<?> serviceUnavailableException(Exception exception) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body( responseModel.responseModel(HttpStatus.INTERNAL_SERVER_ERROR.value(),exception.getMessage()));
    }*/



}
