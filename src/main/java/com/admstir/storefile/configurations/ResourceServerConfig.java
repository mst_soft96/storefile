package com.admstir.storefile.configurations;

import com.admstir.storefile.configurations.exceptions.RESTAuthenticationSuccessHandler;
import com.admstir.storefile.configurations.exceptions.RestAccessDeniedHandler;
import com.admstir.storefile.configurations.exceptions.RestAuthenticationEntryPoint;
import com.admstir.storefile.configurations.exceptions.RestAuthenticationFailureHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.web.AuthenticationEntryPoint;

import static com.admstir.storefile.GlobalVariables.APP_VERSION;


/**
 * @author Mostafa Moradi
 * @version 2.0.0
 * @since 2020/8/20
 */
@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {


    @Override
    public void configure(HttpSecurity http) throws Exception {


        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/oauth/token", "/swagger-ui.html", "/webjars/**", "/swagger-resources/**", "/v2/api-docs", "/resources/**","/api/v1/users/register")
                .permitAll()
                .and()
                .authorizeRequests().antMatchers("/**")
                .hasAnyAuthority("SUPER_ADMIN", "ADMIN_EMPLOYEE")
                .anyRequest().authenticated();


    }

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {


        resources.authenticationEntryPoint(restAuthenticationEntryPoint())
                .accessDeniedHandler(accessDeniedHandler());

    }

    @Bean
    RestAccessDeniedHandler accessDeniedHandler() {
        return new RestAccessDeniedHandler();
    }

    @Bean
    AuthenticationEntryPoint restAuthenticationEntryPoint() {
        return new RestAuthenticationEntryPoint();
    }

    @Bean
    RESTAuthenticationSuccessHandler restAuthenticationSuccessHandler() {
        return new RESTAuthenticationSuccessHandler();
    }

    @Bean
    RestAuthenticationFailureHandler restAuthenticationFailureHandler() {
        return new RestAuthenticationFailureHandler();
    }
}
