package com.admstir.storefile.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Column;
import java.util.List;

/**
 * @author Mostafa Moradi
 * @version 1.0.0
 * @since 2021/8/17
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Document(collection = "users")
public class User extends AuditModel {

    private String name;
    @Indexed(unique=true, sparse=true)//added config to yml file
    private String username;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;
    private List<String> roles;

    private List<String> filesId;


    public User(String name, String username, String password, List<String> roles,List<String> filesId) {
        this.name = name;
        this.username = username;
        this.password = password;
        this.roles = roles;
        this.filesId=filesId;
    }


   /* @Override
    public String toString() {
        return "User [id=" + id + ", name=" + name + ", username=" + username + ", password=" + password + ", roles="
                + roles + "]";
    }*/

}
