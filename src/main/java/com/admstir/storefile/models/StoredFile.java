package com.admstir.storefile.models;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Mostafa Moradi
 * @version 1.0.0
 * @since 2021/8/17
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Document(collation = "storedFile")
public class StoredFile extends AuditModel {
    private String name;
    private String format;
    private String username;

    public StoredFile(String name,String format,String username) {
        this.name = name;
        this.format=format;
        this.username=username;
    }


}
