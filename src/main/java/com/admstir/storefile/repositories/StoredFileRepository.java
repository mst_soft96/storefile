package com.admstir.storefile.repositories;

import com.admstir.storefile.models.StoredFile;
import com.admstir.storefile.models.User;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Mostafa Moradi
 * @version 1.0.0
 * @since 2021/8/17
 */

@Repository
public interface StoredFileRepository extends MongoRepository<StoredFile,String> {

}
