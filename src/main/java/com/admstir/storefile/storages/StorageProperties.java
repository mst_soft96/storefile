package com.admstir.storefile.storages;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
/**
 * @author Mostafa Moradi
 * @version 1.0.0
 * @since 2021/8/17
 */
@Component
@ConfigurationProperties("storage")
public class StorageProperties {

    private String location = System.getProperty("user.dir")+"\\files";

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}
