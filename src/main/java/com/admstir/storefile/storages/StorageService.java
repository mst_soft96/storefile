package com.admstir.storefile.storages;

import org.springframework.web.multipart.MultipartFile;
import java.nio.file.Path;
/**
 * @author Mostafa Moradi
 * @version 1.0.0
 * @since 2021/8/17
 */
public interface StorageService {
    String store(MultipartFile file,String format);
}
