package com.admstir.storefile.storages;

import com.admstir.storefile.components.Generator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/**
 * @author Mostafa Moradi
 * @version 1.0.0
 * @since 2021/8/17
 */
@Service
public class FileSystemStorageService implements StorageService {

    private final Path rootLocation;
    private final Generator generator;

    @Autowired
    public FileSystemStorageService(StorageProperties properties, Generator generator) {
        this.rootLocation = Paths.get(properties.getLocation());
        this.generator = generator;
    }

    @Override
    public String store(MultipartFile file,String format) {
        String fileName = StringUtils.cleanPath(/*file.getOriginalFilename()*/generator.generateRandomString());
        String fileNameWithFormat=fileName+format;
        try {
            if (file.isEmpty())
                throw new StorageException("Failed to store empty file " + fileNameWithFormat);
            if (fileNameWithFormat.contains(".."))
                // This is a security check
                throw new StorageException("Cannot store file with relative path outside current directory " + fileNameWithFormat);
            try (InputStream inputStream = file.getInputStream()) {
                Files.copy(inputStream, this.rootLocation.resolve(fileNameWithFormat), StandardCopyOption.REPLACE_EXISTING);
            }
        } catch (IOException e) {
            throw new StorageException("Failed to store file " + fileNameWithFormat, e);
        }
        return fileName;
    }


}
