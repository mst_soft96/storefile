package com.admstir.storefile.storages;
/**
 * @author Mostafa Moradi
 * @version 1.0.0
 * @since 2021/8/17
 */
public class StorageException extends RuntimeException {

    public StorageException(String message) {
        super(message);
    }

    public StorageException(String message, Throwable cause) {
        super(message, cause);
    }
}
