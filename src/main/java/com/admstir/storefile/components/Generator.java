package com.admstir.storefile.components;

import org.springframework.stereotype.Component;

import java.util.UUID;
/**
 * @author Mostafa Moradi
 * @version 1.0.0
 * @since 2021/8/17
 */

@Component
public class Generator {

    public String generateRandomString() {
        return UUID.randomUUID().toString();
    }
}
