package com.admstir.storefile.controllers;

import com.admstir.storefile.models.requests.UserModel;
import com.admstir.storefile.models.responses.ResponseModel;
import com.admstir.storefile.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;
import java.util.stream.Collectors;

import static com.admstir.storefile.GlobalVariables.APP_VERSION;

@RestController
@RequestMapping("/api/" + APP_VERSION + "/users/")
public class UserController {
    private final UserService userService;
    private final ResponseModel responseModel;


    @Autowired
    public UserController(UserService userService, ResponseModel responseModel) {
        this.userService = userService;
        this.responseModel = responseModel;
    }
    @PostMapping(path = "/register")
    public ResponseEntity<?> uploadFile(@Valid @RequestBody UserModel userModel, BindingResult bindingResult) {
        if(!bindingResult.hasErrors()){
           return userService.userSore(userModel);

        }else {
            List<String> errors = bindingResult.getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.toList());
            return responseModel.responseModel(HttpStatus.BAD_REQUEST.value(), errors);
        }


    }
}
