package com.admstir.storefile.controllers;

import com.admstir.storefile.models.responses.ResponseModel;
import com.admstir.storefile.services.FileService;
import com.admstir.storefile.storages.StorageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.Collections;

import static com.admstir.storefile.GlobalVariables.*;
/**
 * @author Mostafa Moradi
 * @version 1.0.0
 * @since 2021/8/17
 */

@RestController
@RequestMapping("/api/" + APP_VERSION + "/files/")

public class FileController {

    private final StorageProperties storageProperties;
    private final FileService fileService;
    private final ResponseModel responseModel;

    @Autowired
    public FileController(StorageProperties storageProperties, FileService storeService, ResponseModel responseModel) {
        this.storageProperties = storageProperties;
        this.fileService = storeService;
        this.responseModel = responseModel;
    }

    @PostMapping(path = "/upload", consumes = "multipart/form-data")
    public ResponseEntity<?> uploadFile(@RequestPart("fileDocument") MultipartFile fileDocument, Authentication authentication) {

        if (!fileDocument.isEmpty()) {
            return fileService.storeFile(fileDocument,authentication);

        } else
            return responseModel.responseModel(HttpStatus.BAD_REQUEST.value(), Collections.singletonList("File is empty"));
    }

    @GetMapping(value = "/{id}")
    public void downloadFile(HttpServletResponse response, @PathVariable String id,@RequestParam Boolean base64) throws IOException {
        Path file = Paths.get(storageProperties.getLocation(), fileService.findFileById(id));
        if(base64)
            downloadFileInBase64Format(file,response);
        else
            downloadFileWithoutBase64Format(file,response);


    }
    private void downloadFileInBase64Format(Path file,HttpServletResponse response) throws IOException {
        response.setContentType("text/plain");
        OutputStream wrap = Base64.getEncoder().wrap(response.getOutputStream());
        FileInputStream fis = new FileInputStream(file.toFile());
        int bytes;
        byte[] buffer = new byte[2048];
        while ((bytes=fis.read(buffer)) != -1) {
            wrap.write(buffer, 0, bytes);
        }
        fis.close();
        wrap.close();
    }
    private void downloadFileWithoutBase64Format(Path file,HttpServletResponse response){

        if (Files.exists(file)) {
            try {
                Files.copy(file, response.getOutputStream());
                response.getOutputStream().flush();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }




}
